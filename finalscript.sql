create schema study_db;
use study_db;
create table student
(student_id	 int auto_increment primary key,
student_name varchar(45) not null,
surname varchar(45) not null,
second_name varchar(45)not null,
photo blob null,
autobiograpy blob null,
birth_day date not null,
year_of_entry year not null,
group_id int,
rating int not null,
home_adress varchar(45) not null
);

create table student_group
(group_id int auto_increment primary key,
group_code varchar(45) not null,
profession varchar(45) not null,
squupndia decimal not null 
);

create table discipline
(discipline_id int auto_increment primary key,
discipline_name varchar(45) not null
);

create table teacher
(teacher_id int auto_increment primary key,
teacher_name varchar(45) not null,
teacher_surname varchar(45) not null
);

create table lessons_schedule
(lesson_id int auto_increment primary key,
discipline_id int not null,
teacher_id int not null,
control_type varchar(45) not null
);

create table studied_discipline_resuts
(id int not null unique,
student_id int not null,
lesson_id int not null,
result_of_module_1 int not null,
result_of_module_2 int not null,
semester_grade int not null,
semester_points int not null,
semester_number int not null,

constraint PK_resuts_studentid_lessonid
primary key (student_id, lesson_id)
);

ALTER TABLE student ADD 
	CONSTRAINT fk_student_group FOREIGN KEY 
	(
		group_id
	) REFERENCES student_group (
		group_id
	);
    
ALTER TABLE lessons_schedule ADD 
	CONSTRAINT fk_subject_id FOREIGN KEY 
	(
	    discipline_id
	) REFERENCES discipline (
		discipline_id
	);
    
ALTER TABLE lessons_schedule ADD 
	CONSTRAINT fk_teacher_id FOREIGN KEY 
	(
	    teacher_id
	) REFERENCES teacher(
		teacher_id
	);
    
ALTER TABLE studied_discipline_resuts ADD 
	CONSTRAINT fk_student_id FOREIGN KEY 
	(
		student_id
	) REFERENCES student (
		student_id
	);
    
ALTER TABLE studied_discipline_resuts ADD 
	CONSTRAINT fk_lesson_id FOREIGN KEY 
	(
		lesson_id
	) REFERENCES lessons_schedule (
		lesson_id
	);
    
 -- aded some student_groups   
insert into student_group (group_id, group_code, profession, squupndia)
values(1,'TGV-41','builder', 1500.00);
insert into student_group (group_id, group_code, profession, squupndia)
values(2,'PCB-32','builder', 1000.00);
insert into student_group (group_id, group_code, profession, squupndia)
values(3,'MTT-13','builder', 2000.00);
insert into student_group (group_id, group_code, profession, squupndia)
values(4,'BD-11','builder', 700.00);

 -- aded some students         
insert into student (student_id, student_name, surname, second_name, birth_day, year_of_entry, group_id, rating, home_adress)
values(1,'Stepan','Synoviat', 'Mykolayvych', '19940109', '2011', 1, 95, 'Lviv Karadjych st. 55');
insert into student (student_id, student_name, surname, second_name, birth_day, year_of_entry, group_id, rating, home_adress)
values(2,'Steve','Synoviat', 'Mykolayvych', '19950323', '2014', 3, 55, 'Lviv Okrujna st. 22');   
insert into student (student_id, student_name, surname, second_name, birth_day, year_of_entry, group_id, rating, home_adress)
values(3,'Olena','Pikunova', 'Mykolaivna', '19960616', '2013', 4, 99, 'Sambir Centralna st. 1');   
insert into student (student_id, student_name, surname, second_name, birth_day, year_of_entry, group_id, rating, home_adress)
values(4,'Ostap','Yanyk', 'Igorovych', '19930910', '2010', 1, 50, 'Sokal Svobody st. 119');  

-- aded some discipline 
insert into discipline( discipline_id, discipline_name)
values(1, 'MATH');
insert into discipline( discipline_id, discipline_name)
values(2, 'Сhemistry');
insert into discipline( discipline_id, discipline_name)
values(3, 'Automatization');
insert into discipline( discipline_id, discipline_name)
values(4, 'Algoritms');

-- aded some teachers 
insert into teacher(teacher_id, teacher_name, teacher_surname)
values(1, 'Volodymur', 'Musii');
insert into teacher(teacher_id, teacher_name, teacher_surname)
values(2, 'Igor', 'Yarosh');
insert into teacher(teacher_id, teacher_name, teacher_surname)
values(3, 'Igor', 'Yarosh');
insert into teacher(teacher_id, teacher_name, teacher_surname)
values(4, 'Oleksandr', 'Suhara');
insert into teacher(teacher_id, teacher_name, teacher_surname)
values(5, 'Oleg', 'Liashko');

-- aded some lessons
insert into lessons_schedule(lesson_id, discipline_id, teacher_id, control_type)
values(1, 1, 1, 'exam');
insert into lessons_schedule(lesson_id, discipline_id, teacher_id, control_type)
values(2, 1, 5, 'test');
insert into lessons_schedule(lesson_id, discipline_id, teacher_id, control_type)
values(3, 2, 2, 'test');
insert into lessons_schedule(lesson_id, discipline_id, teacher_id, control_type)
values(4, 3, 3, 'exam');
insert into lessons_schedule(lesson_id, discipline_id, teacher_id, control_type)
values(5, 4, 4, 'exam');

-- aded some student_results
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(1, 1, 1 , 44, 32, 76, 4, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(2, 3, 1 , 23, 12, 35, 2, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(3, 2, 4 , 12, 47, 59, 3, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(4, 4, 4 , 33, 29, 62, 3, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(5, 1, 3 , 50, 32, 82, 4, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(6, 2, 1 , 31, 42, 73, 4, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(7, 4, 1 , 29, 36, 65, 3, 1);
insert into studied_discipline_resuts(id, student_id, lesson_id, result_of_module_1, result_of_module_2, semester_grade, semester_points, semester_number)
values(8, 4, 2 , 9, 15, 24, 2, 1);

